//
//  NSString+HexToBytes.h
//  BigInteger
//
//  Created by Soto on 14/11/9.
//  Copyright (c) 2016年 Soto.Poul All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HexToBytes)

- (NSData *)hexToBytes;

@end
